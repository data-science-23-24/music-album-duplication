from types import NoneType
from typing import List
import xml.etree.ElementTree as ET
import numpy as np
import pandas as pd
from fast_edit_distance import edit_distance
import Levenshtein


def sum_two_numbers(x: int, y: int):
    return x + y


def get_text_or_default(element) -> str | None:
    if element is not None and element.text is not None:
        return element.text.strip()
    return None


def getDiscInformationFromXML(path: str) -> pd.DataFrame:
    tree = ET.parse(path)
    root = tree.getroot()

    discs_data = []

    for disc in root.findall('disc'):
        did = get_text_or_default(disc.find('did'))
        artist = get_text_or_default(disc.find('artist'))
        dtitle = get_text_or_default(disc.find('dtitle'))
        category = get_text_or_default(disc.find('category'))
        genre = get_text_or_default(disc.find('genre'))
        year = get_text_or_default(disc.find('year'))
        cdextra = get_text_or_default(disc.find('cdextra'))

        track_elements = disc.find('tracks')
        if track_elements is not None:
            tracks = [
                track.text for track in track_elements if track.text is not None]
        else:
            tracks = []

        discs_data.append([did, artist, dtitle, category,
                          genre, year, cdextra, tracks])

    columns = ['did', 'artist', 'dtitle', 'category',
               'genre', 'year', 'cd extra', 'tracks']
    df = pd.DataFrame(discs_data, columns=columns)
    return df


def getPairsInformationFromXML(path: str) -> pd.DataFrame:
    tree = ET.parse(path)
    root = tree.getroot()

    discs_data = []

    for pair in root.findall('pair'):
        pair_data = []
        for disc in pair.findall('disc'):
            cid = get_text_or_default(disc.find('cid'))
            pair_data.append(cid)

        discs_data.append(pair_data)

    df_duplicates = pd.DataFrame(discs_data, columns=['did1', 'did2'])
    df_duplicates['pair'] = df_duplicates.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)
    df_duplicates = df_duplicates.drop_duplicates(['pair'], keep=False)
    return df_duplicates


def computeMatchingPercentage(ground_truth: pd.DataFrame, predictions: pd.DataFrame) -> float:
    ground_truth['pair'] = ground_truth.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)
    predictions['pair'] = predictions.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)

    predictions = predictions.sort_values(
        by=['matching_score'], ascending=[False])

    ground_truth = ground_truth.drop_duplicates(['pair'], keep='first')
    predictions = predictions.drop_duplicates(['pair'], keep='first')

    matching_pairs_count = len(
        set(ground_truth['pair']).intersection(set(predictions['pair'])))

    max_len = max(len(ground_truth), len(predictions))

    match_percentage = (matching_pairs_count / max_len) * 100
    return match_percentage


def computePredictionInclusionPercentage(ground_truth: pd.DataFrame, predictions: pd.DataFrame) -> float:
    ground_truth['pair'] = ground_truth.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)
    predictions['pair'] = predictions.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)

    predictions = predictions.sort_values(
        by=['matching_score'], ascending=[False])

    ground_truth = ground_truth.drop_duplicates(['pair'], keep='first')
    predictions = predictions.drop_duplicates(['pair'], keep='first')

    matching_pairs_count = len(
        set(ground_truth['pair']).intersection(set(predictions['pair'])))

    total_predictions = len(predictions)

    if total_predictions > 0:
        match_percentage = (matching_pairs_count / total_predictions) * 100
    else:
        match_percentage = 0.0

    return match_percentage


def compute_confusion_matrix_elements(ground_truth: pd.DataFrame, predictions: pd.DataFrame) -> dict:
    ground_truth['pair'] = ground_truth.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)
    predictions['pair'] = predictions.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)

    predictions = predictions.sort_values(
        by=['matching_score'], ascending=[True])

    ground_truth = ground_truth.drop_duplicates(['pair'], keep='first')
    predictions = predictions.drop_duplicates(['pair'], keep='first')

    gt_set = set(ground_truth['pair'])
    pred_set = set(predictions['pair'])

    tp = len(gt_set.intersection(pred_set))
    fp = len(pred_set - gt_set)
    fn = len(gt_set - pred_set)
    tn = 9763**2 - tp - fp - fn

    return {'TP': tp, 'FP': fp, 'FN': fn, 'TN': tn}


def compute_false_negatives_set(ground_truth: pd.DataFrame, predictions: pd.DataFrame) -> set:
    ground_truth['pair'] = ground_truth.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)
    predictions['pair'] = predictions.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)

    predictions = predictions.sort_values(
        by=['matching_score'], ascending=[True])

    ground_truth = ground_truth.drop_duplicates(['pair'], keep='first')
    predictions = predictions.drop_duplicates(['pair'], keep='first')

    gt_set = set(ground_truth['pair'])
    pred_set = set(predictions['pair'])

    # Calculate False Negatives set
    fn_set = gt_set - pred_set

    return fn_set


def compute_false_positives_set(ground_truth: pd.DataFrame, predictions: pd.DataFrame) -> set:
    ground_truth['pair'] = ground_truth.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)
    predictions['pair'] = predictions.apply(
        lambda row: tuple(sorted([row['did1'], row['did2']])), axis=1)

    predictions = predictions.sort_values(
        by=['matching_score'], ascending=[True])

    ground_truth = ground_truth.drop_duplicates(['pair'], keep='first')
    predictions = predictions.drop_duplicates(['pair'], keep='first')

    gt_set = set(ground_truth['pair'])
    pred_set = set(predictions['pair'])

    fp_set = pred_set - gt_set

    return fp_set


def normalized_levenshtein_distance(str1: str | None, str2: str | None) -> float:
    if str1 is None or str2 is None:
        return 0
    distance = Levenshtein.distance(str1, str2)
    max_length = max(len(str1), len(str2))
    if max_length == 0:
        return 0
    if max_length == distance:
        return 0.01
    normalized_distance = 1 - distance / max_length
    return normalized_distance


def normalized_levenshtein_distance_case_insensitive(str1: str | None, str2: str | None) -> float:
    if str1 is None or str2 is None:
        return 0
    distance = Levenshtein.distance(str1.lower(), str2.lower())
    max_length = max(len(str1), len(str2))
    if max_length == 0:
        return 0
    if max_length == distance:
        return 0.01
    normalized_distance = 1 - distance / max_length
    return normalized_distance


def compute_matching_score(record1: pd.Series, record2: pd.Series) -> float:
    if record1.equals(record2):
        return 1
    else:
        scores = {
            'artist_score': normalized_levenshtein_distance(record1['artist'], record2['artist']),
            # 'category_score': normalized_levenshtein_distance(record1['category'], record2['category']),
            'genre_score': normalized_levenshtein_distance(record1['genre'], record2['genre']),
            'title_score': normalized_levenshtein_distance(record1['dtitle'], record2['dtitle']),
            'year_score': normalized_levenshtein_distance(record1['year'], record2['year']),
            'tracks_score': normalized_levenshtein_distance(
                convert_tracks_list_to_string(record1['tracks']),
                convert_tracks_list_to_string(record2['tracks'])
            )
        }

        weights = {
            'artist_score': 0.2,
            # 'category_score': 0.05,
            'genre_score': 0.1,
            'title_score': 0.2,
            'year_score': 0.1,
            'tracks_score': 0.5
        }

        weighted_sum = sum(scores[score] * weights[score] for score in scores)

        return weighted_sum


def compute_matching_score_adapt_zeros(record1: pd.Series, record2: pd.Series) -> float:
    if record1.equals(record2):
        return 1
    else:
        scores = {
            'artist_score': normalized_levenshtein_distance(record1['artist'], record2['artist']),
            # 'category_score': normalized_levenshtein_distance(record1['category'], record2['category']),
            'genre_score': normalized_levenshtein_distance(record1['genre'], record2['genre']),
            'title_score': normalized_levenshtein_distance(record1['dtitle'], record2['dtitle']),
            'year_score': normalized_levenshtein_distance(record1['year'], record2['year']),
            'tracks_score': normalized_levenshtein_distance(
                convert_tracks_list_to_string(record1['tracks']),
                convert_tracks_list_to_string(record2['tracks'])
            )
        }

        weights = {
            'artist_score': 0.1,
            # 'category_score': 0.1,
            'genre_score': 0.1,
            'title_score': 0.3,
            'year_score': 0.1,
            'tracks_score': 0.4
        }

        non_zero_scores = {score: value for score,
                           value in scores.items() if value > 0}
        non_zero_weights = {score: weights[score]
                            for score in non_zero_scores.keys()}

        total_weight = sum(non_zero_weights.values())
        adjusted_weights = {
            score: weight / total_weight for score, weight in non_zero_weights.items()}

        weighted_sum = sum(
            non_zero_scores[score] * adjusted_weights[score] for score in non_zero_scores)

        return weighted_sum


def compute_matching_score_adapt_zeros_new_track_matching(record1: pd.Series, record2: pd.Series) -> float:
    if record1.equals(record2):
        return 1
    else:
        scores = {
            'artist_score': normalized_levenshtein_distance(record1['artist'], record2['artist']),
            # 'category_score': normalized_levenshtein_distance(record1['category'], record2['category']),
            'genre_score': normalized_levenshtein_distance(record1['genre'], record2['genre']),
            'title_score': normalized_levenshtein_distance(record1['dtitle'], record2['dtitle']),
            'year_score': normalized_levenshtein_distance(record1['year'], record2['year']),
            'tracks_score': compute_tracks_average_score(record1['tracks'], record2['tracks'])
        }

        weights = {
            'artist_score': 0.1,
            # 'category_score': 0.1,
            'genre_score': 0.1,
            'title_score': 0.3,
            'year_score': 0.1,
            'tracks_score': 0.4
        }

        non_zero_scores = {score: value for score,
                           value in scores.items() if value > 0}
        non_zero_weights = {score: weights[score]
                            for score in non_zero_scores.keys()}

        total_weight = sum(non_zero_weights.values())
        adjusted_weights = {
            score: weight / total_weight for score, weight in non_zero_weights.items()}

        weighted_sum = sum(
            non_zero_scores[score] * adjusted_weights[score] for score in non_zero_scores)

        return weighted_sum


def compute_matching_score_adapt_zeros_new_track_matching_case_insensitive(record1: pd.Series, record2: pd.Series) -> float:
    if record1.equals(record2):
        return 1
    else:
        scores = {
            'artist_score': normalized_levenshtein_distance_case_insensitive(record1['artist'], record2['artist']),
            # 'category_score': normalized_levenshtein_distance(record1['category'], record2['category']),
            'genre_score': normalized_levenshtein_distance_case_insensitive(record1['genre'], record2['genre']),
            'title_score': normalized_levenshtein_distance_case_insensitive(record1['dtitle'], record2['dtitle']),
            'year_score': normalized_levenshtein_distance_case_insensitive(record1['year'], record2['year']),
            'tracks_score': compute_tracks_average_score_case_insensitive(record1['tracks'], record2['tracks'])
        }

        weights = {
            'artist_score': 0.1,
            # 'category_score': 0.1,
            'genre_score': 0.1,
            'title_score': 0.3,
            'year_score': 0.1,
            'tracks_score': 0.4
        }

        non_zero_scores = {score: value for score,
                           value in scores.items() if value > 0}
        non_zero_weights = {score: weights[score]
                            for score in non_zero_scores.keys()}

        total_weight = sum(non_zero_weights.values())
        adjusted_weights = {
            score: weight / total_weight for score, weight in non_zero_weights.items()}

        weighted_sum = sum(
            non_zero_scores[score] * adjusted_weights[score] for score in non_zero_scores)

        return weighted_sum


def compute_tracks_average_score(tracks1: List[str] | None, tracks2: List[str] | None) -> float:
    if tracks1 is None or tracks2 is None:
        return 0
    scores = []
    for track1 in tracks1:
        track_scores = [normalized_levenshtein_distance(
            track1, track2) for track2 in tracks2]
        if track_scores:
            scores.append(max(track_scores))
    if not scores:
        return 0
    return np.mean(scores)


def compute_tracks_average_score_case_insensitive(tracks1: List[str] | None, tracks2: List[str] | None) -> float:
    if tracks1 is None or tracks2 is None:
        return 0
    scores = []
    for track1 in tracks1:
        track_scores = [normalized_levenshtein_distance_case_insensitive(
            track1, track2) for track2 in tracks2]
        if track_scores:
            scores.append(max(track_scores))
    if not scores:
        return 0
    return np.mean(scores)


def convert_tracks_list_to_string(tracks: list):
    return ''.join(tracks)
